# Moved

For reasons mostly related to difficulties with go.dev's module
cache, bytelog.org/pkg and bytelog.org/cmd have moved. They will now be
located at go.bytelog.org/pkg and go.bytelog.org/cmd, respectively.
