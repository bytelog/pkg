// Derived from golang.org/x/sync/semaphore
//
// Copyright 2017 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package xsync_test

import (
	"runtime"
	"sync"
	"testing"
	"time"

	"bytelog.org/pkg/xsync"
)

func HammerSemaphore(sem *xsync.Semaphore, loops int, done chan<- bool) {
	for i := 0; i < loops; i++ {
		sem.Acquire()
		sem.Release()
	}
	done <- true
}

func TestSemaphore(t *testing.T) {
	sem := xsync.NewSemaphore(1)
	c := make(chan bool)

	for i := 0; i < 10; i++ {
		go HammerSemaphore(&sem, 1000, c)
	}

	for i := 0; i < 10; i++ {
		<-c
	}
}

func TestReleaseFirst(t *testing.T) {
	sem := xsync.Semaphore{}

	sem.Release()
	if !sem.TryAcquire() {
		t.Fatalf("could not acquire semaphore")
	}
	if sem.TryAcquire() {
		t.Fatalf("acquired semaphore")
	}
}

func TestWait(t *testing.T) {
	done := make(chan struct{})
	sem := xsync.NewSemaphore(0)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			sem.Acquire()
			wg.Done()
		}()
		go func() {
			sem.Release()
		}()
	}

	go func() {
		sem.Acquire()
		wg.Wait()
		close(done)
	}()

	select {
	case <-done:
		t.Errorf("did not block indefinitely")
	case <-time.After(time.Millisecond):
	}
}

func TestFairness(t *testing.T) {
	sem := xsync.NewSemaphore(1)
	stop := make(chan bool)
	defer close(stop)
	go func() {
		for {
			sem.Acquire()
			time.Sleep(100 * time.Microsecond)
			sem.Release()
			select {
			case <-stop:
				return
			default:
			}
		}
	}()
	done := make(chan bool)
	go func() {
		for i := 0; i < 10; i++ {
			time.Sleep(100 * time.Microsecond)
			sem.Acquire()
			sem.Release()
		}
		done <- true
	}()
	select {
	case <-done:
	case <-time.After(10 * time.Second):
		t.Fatalf("can't acquire Mutex in 10 seconds")
	}
}

func BenchmarkUncontendedSemaphore(b *testing.B) {
	sem := xsync.NewSemaphore(1)
	HammerSemaphore(&sem, b.N, make(chan bool, 2))
}

func BenchmarkContendedSemaphore(b *testing.B) {
	b.StopTimer()
	sem := xsync.NewSemaphore(1)
	c := make(chan bool)
	defer runtime.GOMAXPROCS(runtime.GOMAXPROCS(2))
	b.StartTimer()

	go HammerSemaphore(&sem, b.N/2, c)
	go HammerSemaphore(&sem, b.N/2, c)
	<-c
	<-c
}
