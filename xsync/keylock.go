package xsync

import (
	"sync"
)

// KeyMutex is an associative mutual exclusion lock.
// The zero value for a KeyMutex contains unlocked keys.
type KeyMutex struct {
	// mu protects access to the lock map.
	//
	// Lock ordering: mu must never be held while acquiring a key-associated
	// lock. When releasing a key-associated lock, mu may be held if the are
	// no pending locks for that key (as indicated by the ref count).
	mu sync.Mutex

	locks  map[string]*locker
	noCopy noCopy
}

// Lock locks a mutex associated with a key.
func (k *KeyMutex) Lock(key string) {
	k.mu.Lock()
	if k.locks == nil {
		k.locks = make(map[string]*locker)
	}
	lk := k.locks[key]
	if lk == nil {
		lk = &locker{}
		k.locks[key] = lk
	}
	lk.refs++
	k.mu.Unlock()
	lk.mu.Lock()
}

// Unlock unlocks a mutex associated with key. It is a runtime error if the
// associated mutex is not locked on entry to Unlock.
func (k *KeyMutex) Unlock(key string) {
	k.mu.Lock()

	if k.locks == nil {
		// Lock() has never been called for any key.
		k.misuse()
		return
	}

	lk := k.locks[key]
	if lk == nil {
		// key is not locked
		k.misuse()
		return
	}

	lk.refs--
	if lk.refs == 0 {
		delete(k.locks, key)
		// Although this looks like a lock-ordering bug, its safe to unlock the
		// key first because there are no remaining references to the key,
		// except our own.
		lk.mu.Unlock()
		k.mu.Unlock()
		return
	}

	k.mu.Unlock()
	lk.mu.Unlock()
}

// misuse mimics the sync.Mutex misuse behavior.
func (k *KeyMutex) misuse() {
	(&sync.Mutex{}).Unlock()
}

type locker struct {
	// refs counts how many references to the locker's key exist. refs is
	// guarded by the global mutex, not the local key mutex.
	refs int

	// mu is the mutex for the key associated to this locker.
	mu sync.Mutex
}
