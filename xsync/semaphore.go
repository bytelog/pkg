package xsync

import (
	_ "unsafe"
)

//go:linkname semacquire sync.runtime_Semacquire
func semacquire(addr *uint32)

//go:linkname semrelease sync.runtime_Semrelease
func semrelease(addr *uint32, handoff bool, skipframes int)

//go:noescape
//go:linkname semtry runtime.cansemacquire
func semtry(addr *uint32) bool

// noCopy can be embedded in a struct to hint go vet to warn on a copy.
// https://github.com/golang/go/issues/8005#issuecomment-190753527
type noCopy struct{}

func (*noCopy) Lock()   {}
func (*noCopy) Unlock() {}

// New creates a new semaphore with the given count of tickets.
func NewSemaphore(tickets uint32) Semaphore {
	return Semaphore{tickets: tickets}
}

// Semaphore provides counted mutual exclusion. The zero value for a Semaphore
// is ready for use. A Semaphore must not be copied after first use.
//
// Unlike x/sync/semaphore, it is valid to release tickets after initialization.
type Semaphore struct {
	tickets uint32
	noCopy  noCopy
}

// Acquire a ticket, blocking until one is available.
func (s *Semaphore) Acquire() {
	semacquire(&s.tickets)
}

// Release adds a ticket and signals the next waiter. There is a maximum of
// 1<<32 tickets, at which point Release() will overflow. Callers are
// responsible for avoiding this overflow.
func (s *Semaphore) Release() {
	// As a counted concurrency primitive, we want to prioritize the happy path
	// and fairness. Always assume starvation mode.
	semrelease(&s.tickets, true, 1)
}

// TryAcquire attempts to acquire a ticket without blocking. On success,
// returns true. On failure, returns false and leaves the semaphore unchanged.
func (s *Semaphore) TryAcquire() bool {
	return semtry(&s.tickets)
}
