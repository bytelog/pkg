// Derived from golang.org/src/sync/mutex_test.go
//
// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package xsync

import (
	"runtime"
	"testing"
	"time"
)

func HammerMutex(m *KeyMutex, loops int, cdone chan bool) {
	for i := 0; i < loops; i++ {
		m.Lock("")
		m.Unlock("")
	}
	cdone <- true
}

func TestSingleMutex(t *testing.T) {
	if n := runtime.SetMutexProfileFraction(1); n != 0 {
		t.Logf("got mutexrate %d expected 0", n)
	}

	defer runtime.SetMutexProfileFraction(0)
	m := new(KeyMutex)
	c := make(chan bool)

	for i := 0; i < 10; i++ {
		go HammerMutex(m, 1000, c)
	}

	for i := 0; i < 10; i++ {
		<-c
	}
}

func TestSingleMutexFairness(t *testing.T) {
	var mu KeyMutex
	stop := make(chan bool)
	defer close(stop)

	go func() {
		for {
			mu.Lock("")
			time.Sleep(100 * time.Microsecond)
			mu.Unlock("")
			select {
			case <-stop:
				return
			default:
			}
		}
	}()
	done := make(chan bool)
	go func() {
		for i := 0; i < 10; i++ {
			time.Sleep(100 * time.Microsecond)
			mu.Lock("")
			mu.Unlock("")
		}
		done <- true
	}()

	select {
	case <-done:
	case <-time.After(10 * time.Second):
		t.Fatalf("can't acquire Mutex in 10 seconds")
	}
}

func BenchmarkMutexThrash(b *testing.B) {
	var mu KeyMutex
	for i := 0; i < b.N; i++ {
		mu.Lock("")
		mu.Unlock("")
	}
}
