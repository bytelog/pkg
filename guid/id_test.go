package guid

import (
	"bytes"
	"testing"

	"golang.org/x/exp/rand"
)

type codingTest struct {
	hi, lo uint64
	text   []byte
	bin    []byte
}

var codingTests = []codingTest{
	{hi: 14697929703826476783, lo: 5591422465364813936, text: []byte("PFSOICAI7L7EUJCON4DOQDB8E0"), bin: []byte{203, 249, 137, 49, 82, 61, 78, 239, 77, 152, 185, 27, 141, 53, 104, 112}},
	{hi: 74029666500212977, lo: 8088122161323000979, text: []byte("043G35N6INSF2S1UP10CB7Q4IC"), bin: []byte{1, 7, 1, 150, 230, 149, 248, 241, 112, 62, 200, 64, 197, 159, 68, 147}},
	{hi: 16521829690994476282, lo: 10814004662382438494, text: []byte("SL4L94AB792FL5GJ1VP09E98BO"), bin: []byte{229, 73, 84, 145, 75, 58, 68, 250, 150, 19, 15, 242, 4, 185, 40, 94}},
	{hi: 9052198920789078554, lo: 7381380909356947872, text: []byte("FMFTTT9LPQP1KPJFTRA2S4GPK0"), bin: []byte{125, 159, 222, 245, 53, 206, 178, 26, 102, 111, 238, 212, 46, 18, 25, 160}},
	{hi: 10961594741481288303, lo: 12502116868085730778, text: []byte("J0FMGLP1P0P6VBC0E46MTAQDR8"), bin: []byte{152, 31, 104, 87, 33, 200, 50, 111, 173, 128, 113, 13, 110, 171, 77, 218}},
	{hi: 16285795259516428329, lo: 6715870808026712034, text: []byte("S81C905G6UG2IN9JI3LERM87S8"), bin: []byte{226, 2, 196, 128, 176, 55, 160, 41, 93, 51, 144, 234, 237, 217, 7, 226}},
	{hi: 528819992478005418, lo: 2284534088986354339, text: []byte("0TBBTUPPOQSAK7TK9EJ66JB2KC"), bin: []byte{7, 86, 190, 251, 57, 198, 184, 170, 31, 180, 75, 166, 99, 77, 98, 163}},
	{hi: 10169200759946765890, lo: 3813019469742317492, text: []byte("HKG44DJ289J44D7AI40MF8SVMG"), bin: []byte{141, 32, 66, 54, 98, 66, 102, 66, 52, 234, 145, 1, 103, 163, 159, 180}},
	{hi: 10592760183762258614, lo: 7367238674766648970, text: []byte("IC0GMGT81K5BCPHTM259HV2MH8"), bin: []byte{147, 1, 11, 67, 168, 13, 10, 182, 102, 61, 176, 138, 152, 252, 86, 138}},
	{hi: 8217673022687244206, lo: 3185531743396549562, text: []byte("E85GK4PLILNQSB1L90V33OEJN8"), bin: []byte{114, 11, 10, 19, 53, 149, 111, 174, 44, 53, 72, 62, 49, 225, 211, 186}},
}

func TestMarshalText(t *testing.T) {
	for i, tc := range codingTests {
		id := ID{hi: tc.hi, lo: tc.lo}
		b, err := id.MarshalText()
		if err != nil {
			t.Errorf("#%d: err: %s", i, err)
		} else if !bytes.Equal(b, tc.text) {
			t.Errorf("#%d: got:%q want:%q", i, b, tc.text)
		}
	}
}

func TestMarshalBinary(t *testing.T) {
	for i, tc := range codingTests {
		id := ID{hi: tc.hi, lo: tc.lo}
		b, err := id.MarshalBinary()
		if err != nil {
			t.Errorf("#%d: err: %s", i, err)
		} else if !bytes.Equal(b, tc.bin) {
			t.Errorf("#%d: got:%q want:%q", i, b, tc.bin)
		}
	}
}

func TestUnmarshalText(t *testing.T) {
	for i, tc := range codingTests {
		var id ID
		if err := id.UnmarshalText(tc.text); err != nil {
			t.Errorf("#%d: err: %s", i, err)
			continue
		}
		if tc.hi != id.hi {
			t.Errorf("#%d: got:%d want:%d", i, id.hi, tc.hi)
			continue
		}
		if tc.lo != id.lo {
			t.Errorf("#%d: got:%d want:%d", i, id.lo, tc.lo)
			continue
		}
	}
}

func TestUnmarshalBinary(t *testing.T) {
	for i, tc := range codingTests {
		var id ID
		if err := id.UnmarshalBinary(tc.bin); err != nil {
			t.Errorf("#%d: err: %s", i, err)
			continue
		}
		if tc.hi != id.hi {
			t.Errorf("#%d: got:%d want:%d", i, id.hi, tc.hi)
			continue
		}
		if tc.lo != id.lo {
			t.Errorf("#%d: got:%d want:%d", i, id.lo, tc.lo)
			continue
		}
	}
}

func TestZero(t *testing.T) {
	if !Zero.IsZero() {
		t.Errorf("Zero.IsZero() false")
	}
	if id := New(); id.IsZero() {
		t.Errorf("New ID.IsZero(). id: %s", id)
	}
}

var (
	sinkID    ID
	sinkBytes []byte
)

func BenchmarkNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sinkID = New()
	}
}

func BenchmarkNewCustomGenerator(b *testing.B) {
	gen := NewGenerator(rand.NewSource(0))
	for i := 0; i < b.N; i++ {
		sinkID = gen.ID()
	}
}

func BenchmarkMarshalText(b *testing.B) {
	b.StopTimer()
	var ids []ID
	for i := 0; i < max(b.N, 10_000); i++ {
		ids = append(ids, New())
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		sinkBytes, _ = ids[i%10_000].MarshalText()
	}
}

func BenchmarkMarshalBinary(b *testing.B) {
	b.StopTimer()
	var ids []ID
	for i := 0; i < max(b.N, 10_000); i++ {
		ids = append(ids, New())
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		sinkBytes, _ = ids[i%10_000].MarshalBinary()
	}
}

func BenchmarkUnmarshalText(b *testing.B) {
	b.StopTimer()
	var ids [][]byte
	for i := 0; i < max(b.N, 10_000); i++ {
		id, _ := New().MarshalText()
		ids = append(ids, id)
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		_ = sinkID.UnmarshalText(ids[i%10_000])
	}
}

func BenchmarkUnmarshalBinary(b *testing.B) {
	b.StopTimer()
	var ids [][]byte
	for i := 0; i < max(b.N, 10_000); i++ {
		id, _ := New().MarshalBinary()
		ids = append(ids, id)
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		_ = sinkID.UnmarshalBinary(ids[i%10_000])
	}
}

func max(a, b int) int {
	if a < b {
		return b
	}
	return a
}
