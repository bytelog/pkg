package guid_test

import (
	"fmt"

	"bytelog.org/pkg/guid"
	"golang.org/x/exp/rand"
)

func ExampleNew() {
	gen := guid.NewGenerator(rand.NewSource(0))
	id := gen.ID()

	fmt.Println(id)

	// Output:
	// PFSOICAI7L7EUJCON4DOQDB8E0
}
