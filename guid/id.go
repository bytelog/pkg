// Package guid facilitates the creation of Globally Unique Identifiers (GUIDs).
package guid

import (
	crand "crypto/rand"
	"encoding/base32"
	"encoding/binary"
	"errors"
	"fmt"
	"sync"

	"golang.org/x/exp/rand"
)

// Zero is the zero value for an ID.
var Zero ID

const size = 16

var (
	b32hex      = base32.HexEncoding.WithPadding(base32.NoPadding)
	encodedSize = b32hex.EncodedLen(size)

	mu         sync.Mutex
	globalRand rand.Source
)

func init() {
	// Obtain a seed from the system random source. /x/exp/rand uses PCG, which
	// requires a good seed to avoid correlation.
	var buf [8]byte
	_, err := crand.Read(buf[:])
	if err != nil {
		panic(fmt.Sprintf("guid.init: could not read seed data: %s", err))
	}
	seed := binary.BigEndian.Uint64(buf[:])
	globalRand = rand.NewSource(seed)
}

// ID represents a globally unique identifier (GUID).
//
// An ID is composed of 128 random bits. Assuming no implementation problems
// with the random number generator used it its construction, the probability
// of two identical IDs existing can be computed as a birthday problem:
//
//     p(n; m) ~= 1 - ((m - 1)/m)^((n/2)*(n-1))
//
// Where n is the number of IDs in existence and m is 2^128, representing the
// total number of possible IDs.
//
// Thus, for even a 1% probability of collision, 2.6 quintillion IDs would need
// to exist.
type ID struct {
	hi uint64
	lo uint64
}

// New creates a new ID using the default generator.
//
// The default generator has good statistical properties, but is not suited
// for security-sensitive usage. For such applications, use NewGenerator with a
// a cryptographically secure pseudorandom number generator to generate IDs.
func New() ID {
	mu.Lock()
	id := ID{globalRand.Uint64(), globalRand.Uint64()}
	mu.Unlock()
	return id
}

// IsZero reports whether id represents the zero ID.
func (id ID) IsZero() bool {
	return id == ID{}
}

// String returns implements the fmt.Stringer interface.
// Provided as a convenience wrapper over MarshalText.
func (id ID) String() string {
	b, _ := id.MarshalText()
	return string(b)
}

// MarshalText implements the encoding.TextMarshaler interface.
// The ID is formatted according to RFC-4648 base32.
func (id ID) MarshalText() ([]byte, error) {
	var src [size]byte
	binary.BigEndian.PutUint64(src[:8], id.hi)
	binary.BigEndian.PutUint64(src[8:], id.lo)

	dst := make([]byte, encodedSize)
	b32hex.Encode(dst, src[:])
	return dst, nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
// The ID is expected to be in RFC-4648 base32 format.
func (id *ID) UnmarshalText(b []byte) error {
	if len(b) == 0 {
		return errors.New("ID.UnmarshalText: no data")
	}
	if len(b) != encodedSize {
		return errors.New("ID.UnmarshalText: invalid length")
	}
	var tmp [size]byte
	if _, err := b32hex.Decode(tmp[:], b); err != nil {
		return err
	}
	*id = ID{
		hi: binary.BigEndian.Uint64(tmp[:8]),
		lo: binary.BigEndian.Uint64(tmp[8:]),
	}
	return nil
}

// UnmarshalBinary implements the encoding.BinaryUnmarshaler interface.
func (id *ID) UnmarshalBinary(b []byte) error {
	if len(b) == 0 {
		return errors.New("ID.UnmarshalBinary: no data")
	}
	if len(b) != size {
		return errors.New("ID.UnmarshalBinary: invalid length")
	}
	*id = ID{
		hi: binary.BigEndian.Uint64(b[:8]),
		lo: binary.BigEndian.Uint64(b[8:]),
	}
	return nil
}

// MarshalBinary implements the encoding.BinaryMarshaler interface.
func (id ID) MarshalBinary() ([]byte, error) {
	var buf [size]byte
	binary.BigEndian.PutUint64(buf[:8], id.hi)
	binary.BigEndian.PutUint64(buf[8:], id.lo)
	return buf[:], nil
}

// A Generator is a source of IDs.
type Generator struct {
	src rand.Source
}

// NewGenerator creates a new Generator from a random Source.
// The uniqueness of an ID is a function of the quality of its Source. A PRNG
// should have, at minimum, a period of 2^128. For security
func NewGenerator(src rand.Source) *Generator {
	return &Generator{src: src}
}

// ID returns a newly created ID.
func (g *Generator) ID() ID {
	return ID{g.src.Uint64(), g.src.Uint64()}
}
